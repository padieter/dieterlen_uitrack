﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour
{
    public Animator dialog;

    public void StartGame()
    {
        SceneManager.LoadScene("Level 1");
    }

    public void OpenSettings()
    {
        dialog.SetBool("isHidden", false);
    }

    public void CloseSettings()
    {
        dialog.SetBool("isHidden", true);
    }

    public void LevelSelect()
    {
        SceneManager.LoadScene("Level Select");
    }

    public void Level1()
    {
        SceneManager.LoadScene("Level 1");
    }

    public void Level2()
    {
        SceneManager.LoadScene("Level 2");
    }

    public void Level3()
    {
        SceneManager.LoadScene("Level 3");
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("StartMenu");
    }

}
